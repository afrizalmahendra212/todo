import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  formData = {
    name: '',
    address: '',
  }

  siswas = [
    {name: 'Bambang', address: 'Madiun'},
  ]

  constructor() {}

  addSiswa() {
    let newSiswa = {name: this.formData.name, address: this.formData.address}
    
    this.siswas.push(newSiswa)

    this.formData.name = ''
    this.formData.address = ''
  }

  deleteSiswa(i) {
    this.siswas.splice(i, 1)
  }
}
